# Test

## Available Scripts

This project was made with create-react-app.

## Requeriments

* Nodejs

## How to run ?
If you already have this project locally you only need to run the following scripts:

```sh
    npm install 
    npm start
```
## dependencies
* React router DOM
* Firebase
* Material UI
* validator
