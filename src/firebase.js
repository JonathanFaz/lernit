import * as firebase from "firebase/app";
import "firebase/auth";

const app = firebase.initializeApp({
  apiKey: "AIzaSyCYToVH0CjqnGxN9YnKEa1KUMPjxDYljEE",
  authDomain: "lernit-c5c5c.firebaseapp.com",
  databaseURL: "https://lernit-c5c5c.firebaseio.com",
  projectId: "lernit-c5c5c",
  storageBucket: "lernit-c5c5c.appspot.com",
  messagingSenderId: 691785405295
});
export default app;