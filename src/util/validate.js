import validator from 'validator';

export const validate = (val, rules, connectedValue) => {
  let isValid = true;
  for (let rule in rules) {
    switch (rule) {
      case "isRequired":
        isValid = isValid && !validator.isEmpty(val);
        break;
      case "isEmail":
        isValid = isValid && validator.isEmail(val);
        break;
      case "minLength":
        isValid = isValid && validator.isLength(val, {min: rules[rule]});
        break;
      case "maxLength":
        isValid = isValid && validator.isLength(val, {max: rules[rule]});
        break;
      case "equalTo":
        isValid = isValid &&  validator.equals(val, connectedValue[rule]);
        break;
      case "notNull":
        isValid = isValid &&  val !== null;
        break;
      default:
        isValid = true;
    }
  }
  
  return isValid;
};

