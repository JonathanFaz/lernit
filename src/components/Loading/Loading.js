import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
const useStyles = makeStyles((theme) => ({
    progress: {
        margin: theme.spacing(6),
        color: '#434CE5',
    }
}))
const Loading = (props) => {
    const classes = useStyles();
    return(
        <Grid 
            className={[classes.intro].join(" ")}
            container 
            direction="row"
            justify="center"
            alignItems="center"
            style={{ height: '100vh' }}
        >
            <Grid
                item
                xs={12}
                
            >          
                <CircularProgress className={classes.progress} size={100} thickness={5} />
            </Grid>
        </Grid>
    )
}
export default Loading;
