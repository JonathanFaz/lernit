import React from 'react';

  
function Layout(WrappedComponent) {
    return function (props) {
            return (
            <div>
                
            <WrappedComponent {...props} />
            
            </div>
            )
        }
    
}
export default Layout;
