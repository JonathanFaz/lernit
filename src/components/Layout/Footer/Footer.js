import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
const useStyles = makeStyles((theme) => ({
    footerSectionContainer:{
        label: 'footer',
        display: 'flex',
        alignItems: 'center',
        color: '#fff',
        textAlign: 'end',
        fontSize: '0.7em',
        borderTop: '1px solid #3c759b',
        fontFamily: "'Poiret One', 'sans-serif'",
    },
    footerSection:{
        backgroundColor: '#293347',
        position:  'static',
        left: "auto",
    }, 
    footerIcons: {
        height: '1.7em',
        margin: '5px'
    },
    footerIconColor: {
        fill: 'white'
    },
    footerText: {
        fontFamily: '"Poiret One", Helvetica, sans-serif',
        color: 'white',
        fontSize: '1rem',	
    }
 }));
function Footer(props){
    const classes = useStyles();
    return (
        <Grid 
        container
        className={classes.footerSection}
        direction="row"
        justify="center"
        alignItems="center"
    >
        <Grid 
            item
            xs={12}
        >
        <Typography className={classes.footerText} variant="subtitle2" gutterBottom>
        A project by Jonathan Guzman 
        <span style={{marginLeft: 5}}  role="img" aria-label="emoji">
        ✌️
        </span>
        </Typography>        
        </Grid>
    </Grid>
    )
}
export default Footer;
