import React, {useState} from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import app from '../firebase'
import { validate } from '../util/validate'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    height: "100vh",
    minHeight: "600px",
    alignItems: "center",
    display: "flex"
  },
  tab:{
    width: "50%"
  },
  paper: {
    height: "500px",
    padding: theme.spacing(5),
    color: theme.palette.text.secondary,
  },
  inputAlign: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  submitItem:{
    margin: "0 auto"
  },
  inputItem: {
    margin: "20px 0px"
  },
  loginText:{
    textAlign: "left",
    color: "black"
  },
  grayText: {
    color: "gray"
  },
  blueText: {
    color: "#434CE5"
  },
  fontWeight700: {
    fontWeight: "700"
  }
}));
function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

export default function Login(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [signIn, setSignIn] = useState({
      email: {
        value: "",
        valid: false,
        type: "text",
        validationRules: {
            isRequired: true,
            isEmail: true
        },
        touched: false


      },
      password: {
        value: "",
        valid: false,
        type: "password",
        validationRules: {
            isRequired: true,
            minLength: 6
        },
        touched: false

      }
    })
    const [signUp, setSignUp] = useState({
      email: {
        value: "",
        valid: false,
        type: "text",
        validationRules: {
            isRequired: true,
            isEmail: true
        },
        touched: false


      },
      password: {
        value: "",
        valid: false,
        type: "password",
        validationRules: {
            isRequired: true,
            minLength: 6
        },
        touched: false

      },
      confirmPassword: {
        value: "",
        valid: false,
        type: "password",
        validationRules: {
            isRequired: true,
            minLength: 6,
        },
        touched: false

      }
    })
    function handleChange (key) {
      return (event) => {
         const value = event.target.value
       
         setSignUp({
                 ...signUp,
                 [key]: {
                      ...signUp[key],
                      value: value,
                      valid: validate(
                          value,  
                          signUp[key].validationRules,

                      ),
                      touched: true
                  } 
         })
     }
    }
    function handleChangeSignIn (key) {
      return (event) => {
         const value = event.target.value
       
         setSignIn({
                 ...signIn,
                 [key]: {
                      ...signIn[key],
                      value: value,
                      valid: validate(
                          value,  
                          signIn[key].validationRules,

                      ),
                      touched: true
                  } 
         })
     }
    }
     
    function checkValidationSignIn () {
      const fields = Object.keys(signIn) 
      return fields.some((val)=> signIn[val].valid === false)         
    }
    function checkValidation () {
      const fields = Object.keys(signUp) 
      return fields.some((val)=> signUp[val].valid === false)         
    }
  
    async function singUp(event){
      event.preventDefault();
      const email = signUp.email.value
      const password = signUp.password.value
      try{
        await app
        .auth()
        .createUserWithEmailAndPassword(email, password);
        props.history.push("/")
      }catch(err){
        console.log(err)
      }

    }
    async function handleLogin(event){
      event.preventDefault();
      const email = signIn.email.value
      const password = signIn.password.value
      try{
        await app
        .auth()
        .signInWithEmailAndPassword(email, password);
        props.history.push("/")
      }catch(err){
        console.log(err)
      }

    }
    
    const handleChangeTab = (event, newValue) => {
      setValue(newValue);
    };
    
    return(
        <div className={classes.root} >
            <Grid 
              container 
              direction="row"
              justify="center"
              alignItems="center" 
            >
                <Grid item xs={8} sm={6} md={4} lg={3} xl={2}>
                  <Typography className={[classes.grayText, classes.fontWeight700].join(" ")} variant="h4"  gutterBottom>
                      WE ARE  <span className={classes.blueText}>LERNIT</span>
                  </Typography>
                  
                
                  <Grid  
                    hidden={value !== 0}
                    id={`simple-tabpanel-${0}`}
                    aria-labelledby={`simple-tab-${0}`} 
                    value={value}  
                    className={classes.paper} 
                    elevation={3} 
                  >
                    <Typography className={classes.loginText} variant="h4"  gutterBottom>
                      Login
                    </Typography>
                    <div className={classes.inputAlign}>
                    <form onSubmit={handleLogin}>
                    <TextField
                      className={classes.inputItem}
                      error={!signIn.email.valid  && signIn.email.touched}
                      value={signIn.email.value}
                      name="signInEmail" 
                      onChange={handleChangeSignIn('email')}
                      required
                      label="Email"
                      variant="outlined"
                    />
                    <TextField
                      className={classes.inputItem}
                      error={!signIn.password.valid  && signIn.password.touched}
                      value={signIn.password.value}
                      onChange={handleChangeSignIn('password')}
                      name="signInPassword"
                      required
                      label="Password"
                      type="password"
                      variant="outlined"
                    />
                    

                    <Button style={{margin: "0 auto"}}
                      className={[classes.inputAlign].join("")} type="submit" variant="contained" color="primary"  disabled={checkValidationSignIn()} >
                        Enviar
                    </Button>
                    </form>
                    </div>
                  </Grid>
                  
                  <Grid  
                    hidden={value !== 1}
                    id={`simple-tabpanel-${1}`}
                    aria-labelledby={`simple-tab-${1}`} 
                    value={value}  
                    className={classes.paper} 
                    elevation={3} 
                  >
                    <Typography className={classes.loginText} variant="h4"  gutterBottom>
                      Register
                    </Typography>
                    <div className={classes.inputAlign}>
                    <form onSubmit={singUp}>
                    <TextField
                      error={!signUp.email.valid  && signUp.email.touched}
                      className={classes.inputItem}
                      onChange={handleChange('email')}
                      name="email" 
                      value={signUp.email.value}
                      required
                      id="outlined-required"
                      label="Email"
                      variant="outlined"
                    />
                    <TextField
                      error={!signUp.password.valid  && signUp.password.touched}
                      className={classes.inputItem}
                      value={signUp.password.value}
                      onChange={handleChange('password')}
                      name="password" 
                      required
                      id="outlined-required"
                      label="Password"
                      type="password"
                      variant="outlined"
                    />
                    
                    <TextField
                      error={!signUp.confirmPassword.valid  && signUp.confirmPassword.touched}
                      className={classes.inputItem}
                      value={signUp.confirmPassword.value}
                      onChange={handleChange('confirmPassword')}
                      name="confirmPassword" 
                      required
                      id="outlined-required"
                      label="Password"
                      type="password"
                      variant="outlined"
                    />

                    <Button style={{margin: "0 auto"}}
                      className={[classes.inputAlign].join("")} type="submit" variant="contained" color="primary"  disabled={checkValidation()} >
                        Enviar
                    </Button>

                    </form>
                    </div>
                  </Grid>
               
               
                  <Tabs
                    value={value}
                    onChange={handleChangeTab}
                    orientation="vertical"
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    aria-label="scrollable auto tabs example"
                    
                  >
                    <Tab label="Log In"  className={classes.tab} {...a11yProps(0)} />
                    <Tab label="Sign Up" className={classes.tab} {...a11yProps(1)} />
                  
                  </Tabs>
                </Grid>
            </Grid>
        </div>
    )
}