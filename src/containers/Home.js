import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import AddIcon from '@material-ui/icons/Add';
import app from "../firebase";
import Button from '@material-ui/core/Button';
import ButtonBase from '@material-ui/core/ButtonBase';
const useStyles = makeStyles((theme) => ({
    blankText: {
        color: "#FFF"
    },
    root: {
        height: "100vh",
        flexGrow: 1,
        backgroundColor: "#434CE5"
    },
    buttonSignout:{
        backgroundColor: "#4138CC",
        margin: theme.spacing(5),

    },
    button:{
        backgroundColor: "#FFF",
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(5),
        paddingLeft: theme.spacing(6),
        paddingRight: theme.spacing(6),
        width: "50px",
        height: "50px",
        borderRadius: "15px"

    },
    counterSection: {
        minHeight: "75%",
        paddingLeft: "100px"
    },
    counterButton: {
        height: "50px",
        width: "50px"
    },
    footerSection:{
        backgroundColor: "#FFF",
        minHeight: "25%",
        left: "auto",
        position:  'absolute',
        bottom: 0,
        right: 0,
    }, 
    weight700:{
        fontWeight: "700"
    },
    icon:{
        fontColor: "#434CE5"
    }
}));

export default function Home(props) {
    const classes = useStyles();
    const [counter, setCounter] = React.useState(parseInt(sessionStorage.getItem('counter')) || 0);
    const counterHandler = () => {
        let sum = counter + 1
        setCounter(sum);
        sessionStorage.setItem('counter', sum);
    };
    return(
        <div className={classes.root} >
            <Grid 
              container 
              className={classes.counterSection}
              direction="column"
              justify="center"
              alignItems="flex-start"
            >
                    <Typography className={[classes.weight700, classes.blankText].join(" ")} variant="h3">{counter}</Typography>
                    <Typography className={classes.blankText}>
                        contador
                    </Typography>
                    
                    <ButtonBase onClick={counterHandler} className={classes.button} >                
                    <AddIcon className={classes.icon} fontSize="large"/>
                    </ButtonBase>
                    
                   
            </Grid>
            <Grid 
              container 
              direction="row"
              justify="center"
              alignItems="center" 
              className={classes.footerSection}
            >

                        
                    <Button                
                    variant="contained"
                    color="default"
                    fullWidth={true}
                    className={classes.buttonSignout}
                    onClick={() => app.auth().signOut()}
                    >   
                        <p className={classes.blankText}>
                        Cerrar Session
                        </p>
                    </Button>
                   
            </Grid>
        </div>
    )
}