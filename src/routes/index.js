import React, { useContext, Suspense, lazy } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Redirect } from 'react-router'
import { AuthContext } from '../context/auth'
import Layout from '../components/Layout/Layout'
import Loading from '../components/Loading/Loading'
import Auth from './auth'
import {AuthProvider} from '../context/auth'

const ComponentWrapper = ( Component ) => {
    return props => (
        <Suspense fallback={<Loading />}>
          <Component {...props} />
        </Suspense>
      );
}

const routes = [{
    path: '/',
    component: ComponentWrapper(lazy(() => import('../containers/Home'))),
    layout: Layout,
    roles: {
        requiresAuth: true,
        allowedRoles: []
    },
    config: {
        exact: true
    }
},
{
    path: '/login',
    component: ComponentWrapper(lazy(() => import('../containers/Login'))),
    layout: Layout,
    roles: {
        requiresAuth: false,
        allowedRoles: []
    },
    config: {
        exact: true
    }
},
{
    path: '*',
    component: ComponentWrapper(lazy(() => import('../containers/Error404'))),
    layout: Layout,
    roles: {
        requiresAuth: false,
        allowedRoles: []
    },
    config:{
        exact: true
    }
}]

const PrivateRoute = ({ component: RouteComponent, ...rest }) => {
  const {user} = useContext(AuthContext);
  return (
    <Route
      {...rest}
      render={routeProps =>
        !!user ? (
          <RouteComponent {...routeProps} />
        ) : (
          <Redirect to={"/login"} />
        )
      }
    />
  );
};



const routerWithRoutes = (route) => {
    return (
        <AuthProvider>
        <Router>
            <Switch>
            {
                routes.map(({path, component, roles, layout, config})=>{
                    if(roles.requiresAuth){
                        return <PrivateRoute key={path} path={path} component={Auth(layout(component), roles)} {...config}/>
                    }else{
                        return <Route key={path} path={path} component={Auth(layout(component), roles)} {...config}/>
                    }
                })
            }
            </Switch>
        </Router>
        </AuthProvider>
        
    ) 
}
export default routerWithRoutes
